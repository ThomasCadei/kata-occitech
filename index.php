<?php
require_once 'vendor/autoload.php';

use App\Model\{
    Game,
    Player
};

$player1 = new Player('Roger FEDERER');
$player2 = new Player('Rafael NADAL');
$game = new Game($player1, $player2);
?>

<!DOCTYPE html>
<html>

<head>
    <title>Tennis</title>
    <style>
        body {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }

        th,
        td {
            text-align: center;
            border: 1px solid #000;
            padding: 10px;
        }

        th {
            background-color: #000;
            color: #fff;
        }

        strong {
            font-weight: bold;
            background-color: #ffff00;
            padding: 10px;
        }
        button {
            margin: 20px 0;
        }
    </style>
</head>

<body>
    <button onclick="window.location.reload();">Play again !</button>
    <?php
        echo '<table><theader><tr><th>' . $player1->getName() . '</th><th>' . $player2->getName() . '</th></tr></theader><tbody>';
        while ($game->getWinner() == null) {
            $game->playBall();
            echo '<tr>';
            if ($game->getPoints($player1) == $game->getPoints($player2) && $game->getTieBreak() == null) {
                echo '<td colspan=2>' . $game->displayPoints($player1) . 'A</td>';
            } else {
                echo '<td>' . $game->displayPoints($player1) . '</td><td>' . $game->displayPoints($player2)  . '</td>';
            }
            echo '</tr>';
        }
        echo '</tbody></table><br>';
        echo '<strong>' . $game->getWinner()->getName() . ' wins !</strong>';    
    ?>
</body>

</html>