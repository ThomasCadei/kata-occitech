<?php

namespace App\Model;

class Game
{
    private $player1;
    private $player2;
    const REAL_POINTS = [
        0 => 0,
        1 => 15,
        2 => 30,
        3 => 40,
    ];

    public function __construct(Player $player1, Player $player2)
    {
        $this->player1 = $player1;
        $this->player2 = $player2;
        $this->points1 = 0;
        $this->points2 = 0;
        $this->tieBreak = null;
        $this->winner = null;
    }

    /**
     * Play a ball
     * @return  void
     */
    public function playBall(): void
    {
        $random = rand(0, 1);
        if (isset($this->tieBreak)) {
            $this->matchPoint($random);
        } else {
            $this->incrementPoints($random);
        }
    }

    /**
     * Match point
     * @param int $random
     * @return  void
     */
    private function matchPoint(int $random): void
    {
        switch ($random) {
            case 0:
                $this->tieBreak == $this->player1 ? $this->winner = $this->player1 : $this->tieBreak = $this->player1;
                break;
            case 1:
                $this->tieBreak == $this->player2 ? $this->winner = $this->player2 : $this->tieBreak = $this->player2;
                break;
            default:
                break;
        }
    }

    /**
     * Increment points
     * @param int $random
     * @return  void
     */
    private function incrementPoints(int $random): void
    {
        switch ($random) {
            case 0:
                $this->points1 == 3 ? ($this->points2 != 3 ? $this->winner = $this->player1 : $this->tieBreak = $this->player1) : $this->points1++;
                break;
            case 1:
                $this->points2 == 3 ? ($this->points1 != 3 ? $this->winner = $this->player2 : $this->tieBreak = $this->player2) : $this->points2++;
                break;
            default:
                break;
        }
    }

    /**
     * Get points
     * @param Player $player
     * @return int
     */
    public function getPoints(Player $player): int
    {
        $points = null;
        switch ($player) {
            case $this->player1:
                $points = SELF::REAL_POINTS[$this->points1];
                break;
            case $this->player2:
                $points = SELF::REAL_POINTS[$this->points2];
                break;
            default:
                break;
        }
        return $points;
    }

    /**
     * Return displayed points
     * @param Player $player
     * @return string
     */
    public function displayPoints(Player $player): string
    {
        return $this->winner == $player ? 'JEU' : ($this->tieBreak == $player ? 'AVANTAGE' : $this->getPoints($player));
    }

    /**
     * Get winner
     * @return ?Player
     */
    public function getWinner(): ?Player {
        return $this->winner;
    }

    /**
     * Get tiebreak
     * @return ?Player
     */
    public function getTieBreak(): ?Player {
        return $this->tieBreak;
    }
}
