<?php

namespace App\Model;

class Player {
    
    private $name;

    public function __construct(string $name) {
        $this->name = $name;
    }

    /**
     * Get the value of name
     */ 
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     * @param string $name
     * @return  self
     */ 
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
?>