<?php

namespace App\Test;

use App\Model\{
    Game,
    Player
};
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase {

    public function testGame() {
        $player1 = new Player('Player1');
        $player2 = new Player('Player2');
        new Game($player1, $player2);
        $this->assertTrue(true);
    }

    public function testGetPoints() {
        $player1 = new Player('Player1');
        $player2 = new Player('Player2');
        $game = new Game($player1, $player2);
        $this->assertEquals(0, $game->getPoints($player1));
    }
}