<?php

namespace App\Test;

use App\Model\Player;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase {
    
    public function testPlayer() {
        new Player('Player1');
        $this->assertTrue(true);
    }

    public function testGetName() {
        $player = new Player('Player1');
        $this->assertEquals('Player1', $player->getName());
    }

}